﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteboardPenTip : MonoBehaviour
{
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetColor(Color color)
	{
		GetComponentInParent<MeshRenderer>().material.color = color;
		Debug.Log(GetComponentInParent<MeshRenderer>().material.color);
	}
}
