﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class WhiteboardPen : VRTK_InteractableObject {

    [Header("Brush Options")]
    [SerializeField] private GameObject m_PenTip = default(GameObject);
    [SerializeField] private float m_RaycastLength = 0.01f;
    [SerializeField] private Texture2D m_Brush = default (Texture2D);
    [SerializeField] private PaintMode m_PaintMode = PaintMode.Draw;
    [SerializeField] private Color m_Color;
    [SerializeField] private float m_Spacing = 1f;
    
    private Whiteboard _whiteboard = default(Whiteboard);
    private RaycastHit _touchPoint = default(RaycastHit);
    private Rigidbody _rigidBody = default(Rigidbody);
    private VRTK_ControllerReference _controller = default(VRTK_ControllerReference);
    private bool _lastTouch = false;
    private float _lastAngle = 0f;
    private Vector2? _lastDrawPosition = null;
    private Stamp _stamp;

    protected override void Awake()
    {
        base.Awake();
        m_PenTip.GetComponent<WhiteboardPenTip>().SetColor(m_Color);
    }

    void Start () 
    {
        _stamp = new Stamp(m_Brush);
        _stamp.mode = m_PaintMode;
        _rigidBody = this.GetComponent<Rigidbody>();
        _whiteboard = FindObjectOfType<Whiteboard>();

    }

    void Update () {

        var currentAngle = -transform.rotation.eulerAngles.z;

        var ray = new Ray(m_PenTip.transform.position, m_PenTip.transform.up);
        RaycastHit hit;
        var paintReceiverCollider = _whiteboard.GetComponent<Collider>();
        Debug.DrawRay(ray.origin, ray.direction * m_RaycastLength);

        if (paintReceiverCollider.Raycast(ray, out hit, m_RaycastLength))
        {
            
            VRTK_ControllerHaptics.TriggerHapticPulse(_controller, 0.05f);
            
            if (_lastDrawPosition.HasValue && _lastDrawPosition.Value != hit.textureCoord)
            {
                _whiteboard.DrawLine(_stamp, _lastDrawPosition.Value, hit.textureCoord, _lastAngle, currentAngle, m_Color, m_Spacing);
            }
            else
            {
                _whiteboard.CreateSplash(hit.textureCoord, _stamp, m_Color, currentAngle);
            }

            _lastAngle = currentAngle;

            _lastDrawPosition = hit.textureCoord;
            LockRotation();
        }
        else
        {
            _lastDrawPosition = null;
            UnlockRotation();
        }
    }


    private void LockRotation ()
    {
        _rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
    }


    private void UnlockRotation()
    {
        _rigidBody.constraints = RigidbodyConstraints.None;

    }

    public override void Grabbed(VRTK_InteractGrab currentGrabbingObject = null)
    {
        base.Grabbed(currentGrabbingObject);
        _controller = VRTK_ControllerReference.GetControllerReference(currentGrabbingObject.gameObject);
    }
}
